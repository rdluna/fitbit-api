// initialize the express application
const express = require("express");
const app = express();
var token

// initialize the Fitbit API client
const FitbitApiClient = require("fitbit-node");
const client = new FitbitApiClient({
  clientId: "22D6PV",
  clientSecret: "4544cda8271cc6d7f22ae5868011a507",
  apiVersion: '1.2' // 1.2 is the default
});

const { Client } = require('pg')
const pgsql_client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'postgres',
  port: 5432
})
pgsql_client.connect()


// redirect the user to the Fitbit authorization page
app.get("/authorize", (req, res) => {
  // request access to the user's activity, heartrate, location, nutrion, profile, settings, sleep, social, and weight scopes
  res.redirect(client.getAuthorizeUrl('activity heartrate location nutrition profile settings sleep social weight', 'http://127.0.0.1:3000/callback'));
});

// handle the callback from the Fitbit authorization flow
app.get("/callback", (req, res) => {
  // exchange the authorization code we just received for an access token
  client.getAccessToken(req.query.code, 'http://127.0.0.1:3000/callback').then(result => {
    // use the access token to fetch the user's profile information
    token = result.access_token;
    console.log(token);
    client.get("/profile.json", result.access_token).then(results => {
      res.send(results[0]);
    }).catch(err => {
      res.status(err.status).send(err);
    });
  }).catch(err => {
    res.status(err.status).send(err);
  });
});

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

async function insert_query_json(table, data) {
  var query = "INSERT INTO " + table + " (data) VALUES (json_array_elements('" + data + "')) ON CONFLICT DO NOTHING";
  const output = await pgsql_client.query(query)
    .then(result => {
      return result;
    })
    .catch(e => {
      return e.stack;
    })
  return output
}

async function insert_query(table, columns, values) {
  var query = "INSERT INTO " + table + " (" + columns + ") VALUES (" + values + ") ON CONFLICT DO NOTHING";
  const output = await pgsql_client.query(query)
    .then(result => {
      return result;
    })
    .catch(e => {
      return e.stack;
    })
  return output
}

app.get("/heartrate/:date", function(req, res) {
  client.get("/activities/heart/date/" + req.params.date + "/1d.json", token)
    .then(async function(results) {
      var date = results[0]['activities-heart'][0]['dateTime'];
      var data = JSON.stringify(results[0]['activities-heart-intraday']['dataset']);
      data = data.replaceAll('"time":"', '"dateTime":"' + date + "T");
      result = await insert_query_json('fitbit.heartrate', data);
      res.send(result);
    }).catch(err => {
      res.status(err.status).send(err);
    });
});

app.get("/sleep/:date", function(req, res) {
  client.get("/sleep/date/" + req.params.date + ".json", token)
    .then(async function(results) {
      var jsonSleep = results[0]['sleep'][0]
      var jsonSumm = results[0]['summary']
      var data1 = JSON.stringify(jsonSleep['levels']['data']);
      var result1 = await insert_query_json('fitbit.sleeplevels', data1);
      var data2 = JSON.stringify(jsonSleep['levels']['shortData']);
      var result2 = await insert_query_json('fitbit.sleeplevels', data2);
      var columns = "datetime, duration, efficiency, starttime,	endtime, asleep, awake, bedtime, deep, light,	rem, wake, total";
      var values = [
        "'" + jsonSleep['dateOfSleep'] + "'",
        jsonSleep['duration'],
        jsonSleep['efficiency'],
        "'" + jsonSleep['startTime'] + "'",
        "'" + jsonSleep['endTime'] + "'",
        jsonSleep['minutesAsleep'],
        jsonSleep['minutesAwake'],
        jsonSleep['timeInBed'],
        jsonSumm['stages']['deep'],
        jsonSumm['stages']['light'],
        jsonSumm['stages']['rem'],
        jsonSumm['stages']['wake'],
        jsonSumm['totalMinutesAsleep']
      ];
      var valueStr = values.join(',');
      var result3 = await insert_query('fitbit.sleepsummary', columns, valueStr);
      var result = [result1, result2, result3];
      res.send(result);
    }).catch(err => {
      res.status(err.status).send(err);
    });
});

app.get("/activity/:activity/:date", (req, res) => {
  client.get("/activities/" + req.params.activity + "/date/" + req.params.date + "/1d.json", token)
    .then(async function(results) {
      var date = results[0]['activities-' + req.params.activity][0]['dateTime'];
      var data = JSON.stringify(results[0]['activities-' + req.params.activity + '-intraday']['dataset']);
			data = data.replaceAll('"time":"', '"dateTime":"' + date + "T");
			result = await insert_query_json('fitbit.' + req.params.activity, data);
      res.send(result);
    }).catch(err => {
      res.status(err.status).send(err);
    });
});

// launch the server
app.listen(3000);
console.log("Listening on port 3000")
